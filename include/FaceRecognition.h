
#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing/render_face_detections.h>
#include <dlib/image_processing.h>
//#include <dlib/gui_widgets.h>
#include <dlib/image_io.h>
#include <iostream>

//numpy like C++ for numerical calculation
#include "xtensor-blas/xlinalg.hpp"

//#include "xtensor-io/"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>

#include "FaceModel.h"

using namespace std;
using namespace dlib;
using namespace cv;

#define     xtLinalg            xt::linalg::norm
#define     CVMat               cv::Mat
#define     DLIB_IMAGE          dlib::matrix<dlib::rgb_pixel>


//using defines
#define USE_DLIB_CV
#define USE_CNN

#if defined USE_CNN

template <long num_filters, typename SUBNET> using con5d = con<num_filters,5,5,2,2,SUBNET>;
template <long num_filters, typename SUBNET> using con5  = con<num_filters,5,5,1,1,SUBNET>;
template <typename SUBNET> using downsampler  = relu<affine<con5d<32, relu<affine<con5d<32, relu<affine<con5d<16,SUBNET>>>>>>>>>;
template <typename SUBNET> using rcon5  = relu<affine<con5<45,SUBNET>>>;
using net_type = loss_mmod<con<1,9,9,1,1,rcon5<rcon5<rcon5<downsampler<input_rgb_image_pyramid<pyramid_down<6>>>>>>>>;

#define     NETCNN             net_type

#endif

#if defined USE_DLIB_CV

#define IMAGE_ASSERT            dlib::matrix<dlib::rgb_pixel>

#elif defined USE_OPENCV

#define IMAGE_ASSERT            cv::Mat

#endif

 typedef xt::xarray<double> facetensor;
// using namespace face;
// frontal_face_detector detector = dlib::get_frontal_face_detector();
// face::model faceModel;
// faceModel.poseposeModel68Point();
// face::model posePredictor68Point;
//dlib::full_object_detection

class FaceRecognition
{
private:
    // void init();
    typedef xt::xarray<double> facetensor;
    face::model faceModel;
    frontal_face_detector detector = dlib::get_frontal_face_detector();
//    shape_predictor sp;


public:
    FaceRecognition();
    ~FaceRecognition();

    struct _css
    {
        dlib::rectangle _rect;
        // _rect.

    };

    dlib::rectangle rect2css(dlib::rectangle _rect);
    dlib::rectangle css2rect(dlib::rectangle _rect);

    IMAGE_ASSERT loadImage(char* imageFile);

    auto faceDetection(char* imageFile);

    auto& raw_face_location(char* img, int number_of_times_to_upsample = 1, const std::string model = "hog");

    auto faceLandmarks();
    struct _css2rect
    {

    };

    // xtLinalg face_distance();
    facetensor face_distance();


};
