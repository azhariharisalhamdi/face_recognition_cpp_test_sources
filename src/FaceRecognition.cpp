#pragma once

#include "FaceRecognition.h"

FaceRecognition::FaceRecognition()
{
	;
}

FaceRecognition::~FaceRecognition()
{
	;
}

IMAGE_ASSERT FaceRecognition::loadImage(char* imageFile)
{
  IMAGE_ASSERT temp_image_matrices;
#if defined USE_DLIB_CV
  dlib::load_image(temp_image, imageFile);
  return temp_image_matrices;
#elif defined USE_OPENCV
  temp_image_matrices = cv::imread(imageFile, CV_LOAD_IMAGE_COLOR);
  if(!temp_image_matrices.data)
  {
          cout<< "cannot open path file image : " << imageFile <<endl;
          return -1;
  }
  return temp_image_matrices;
#endif
}

auto& raw_face_location(char* img, int number_of_times_to_upsample = 1, const std::string model = "hog")
{
	if(model == "cnn")
	{
	    auto resnetModel = this.faceModel.resnetModel();
	    std::vector<matrix<float,0,1>> face_descriptors = resnetModel(img);
//	    return
	}
}

auto faceDetection(char* imageFile)
{
#if defined USE_CNN
//  NETCNN net;
  dlib::matrix<dlib::rgb_pixel> image;
  dlib::load_image(image, imageFile);
  while(image.size() < 1800*1800)
    dlib::pyramid_up(image);
  auto detFace = this.faceModel.CNNFaceDetector(image);
  return detFace;
#elif defined USE_HOG
  std::vector<matrix<rgb_pixel>> faces;
  dlib::matrix<dlib::rgb_pixel> image;
  dlib::load_image(image, imageFile);
  auto detFace = this.detector(image);
  return detFace;
#endif
}
